package com.devcamp.s10.task56b20.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin

public class bookController {
     @GetMapping("/book")
     public ArrayList<Book> getBook(){
          Author author1 = new Author("Truong Tan Loc", "Loctt1@devcampVn.edu.vn", 'M');
          Author author2 = new Author("Vuong Thi My Cam", "Camvtmcs160075@fpt.edu.vn", 'F');
          Author author3 = new Author("Truong Phat Tai", "TruongPhatTai@gmail.com", 'M');

           // In thông tin tác giả ra console
          System.out.println(author1.toString());
          System.out.println(author2.toString());
          System.out.println(author3.toString());

             // Khởi tạo 3 đối tượng sách tương ứng với 3 tác giả
          Book book1 = new Book("Book 1", author1, 10000, 2);
          Book book2 = new Book("Book 2", author2, 20000, 3);
          Book book3 = new Book("Book 3", author3, 30000, 3);

           // In thông tin sách ra console
          System.out.println(book1.toString());
          System.out.println(book2.toString());
          System.out.println(book3.toString());

           // Khởi tạo một ArrayList Book mới và thêm các đối tượng sách vào nó
          ArrayList<Book> books = new ArrayList<>();
          books.add(book1);
          books.add(book2);
          books.add(book3);

          return books;
     }
}
